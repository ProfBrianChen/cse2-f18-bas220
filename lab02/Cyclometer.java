public class Cyclometer{ //main method required for every java program
  public static void main(String[] args){ //Our input data
    int secsTrip1=480; //Integer variable of seconds of trip one
    int secsTrip2=3220; //integer variable of seconds of trip two
    int countsTrip1=1561; //INteger variable of counts of trip one
    int countsTrip2=9037; //Integer variable of counts of trip 2
   
    double wheelDiameter=27.0; // Stores thediameter of the wheel ina  double type variable
    double PI = 3.14159; //
    int feetPerMile=5280; //
    int inchesPerFoot=12; //
    int secondsPerMinute=60; //
    double distanceTrip1, distanceTrip2,totalDistance; // 
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts."); //prints 8 minutes
	  System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); // prints 53 minutes
    distanceTrip1=countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile; //gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //gives the distance for trip two
    totalDistance = distanceTrip1 + distanceTrip2; //Adds the two distances for total distance
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); //Prints the distance 1 length
	  System.out.println("Trip 2 was "+distanceTrip2+" miles"); //Prints the distance 2 length 
	  System.out.println("The total distance was "+totalDistance+" miles"); //Prints the total distance
  }
}